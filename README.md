# curious

## git / vim

- [x] [reddit.com/r/vim](https://old.reddit.com/r/vim/comments/lkmh22/how_i_stage_git_commits_within_vim/)  
      stage git single line in vim  
      `:Git`, select file with `=` to open diff, select line with `V` and stage with `-`.
- [ ] [spacevim.org](https://spacevim.org/)  
      Even more IDE vim.
- [ ] [codeberg.org](https://codeberg.org/)  
      Git hosting.
- [ ] [sourcehut.org](https://sourcehut.org/)  
      Git hosting.

## python

- [ ] [github.com/satwikkansal/wtfpython](https://github.com/satwikkansal/wtfpython)  
      wtf strange python behaviour snippets

## layout

- [ ] [bjoernkarmann.dk/occlusion-grotesque](https://bjoernkarmann.dk/occlusion-grotesque)  
      living tree font
- [ ] [docs.dropzone.dev](https://docs.dropzone.dev/configuration/basics/configuration-options)

## security

- [ ] [docs.gitlab.com/../sast](https://docs.gitlab.com/ee/user/application_security/sast/index.html)  
      Static Application Security Testing
- [ ] [objective-see.org](https://objective-see.org/products/knockknock.html)  
      KnockKnock mac persistent malware scanner
- [ ] [mvsp.dev](https://mvsp.dev/)  
      Minimum viable secure product
- [ ] [vanta.com](https://www.vanta.com/guides/vantas-guide-to-soc-2)  
      SOC 2 report
- [ ] [github.com/undergroundwires/privacy.sexy](https://github.com/undergroundwires/privacy.sexy)  
       Scripts to set better privay defaults.

## geo

- [ ] [github.com/minorua/Qgis2threejs](https://github.com/minorua/Qgis2threejs)
      Convert qgis 3d to webbrowser view + 3d printable map.

## government data

- [ ] [gemmaonline.nl](https://www.gemmaonline.nl/index.php/API-standaarden)  
      [vng-realisatie.github.io](https://vng-realisatie.github.io/gemma-zaken/standaard/)  
      ZGW (zaak gericht werken) api

## documentation

- [ ] [divio.com](https://documentation.divio.com/)  
      Types of documentation writing.

## api

- [ ] [ycombinator.com bluethl.net](https://news.ycombinator.com/item?id=30647784)  
      Design rest apis.
- [ ] [google.com](https://cloud.google.com/endpoints/docs/openapi/when-why-api-key)  
      Api keys.
- [ ] [docs.microsoft.com](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design)  
      Best practices.

## data

- [ ] [github.com/psi-4ward/psitransfer](https://github.com/psi-4ward/psitransfer)  
      [gitlab.com/mojo42/Jirafeau](https://gitlab.com/mojo42/Jirafeau)  
      [projectsend.org/](https://www.projectsend.org/)  
      Self-hosted alternatives to wetransfer.

## jupyter

- [ ] [trymito.io](https://www.trymito.io/)  
      Mito excel like.

## diffs

- [ ] [ycombinator.com difftastic](https://news.ycombinator.com/item?id=30841244)  
      difftastic

## vision

- [ ] [nvidia.com](https://www.nvidia.com/research/inpainting/)  
      Inpainting demo.
- [x] [ycombinator.com kuprel/min-dalle](https://news.ycombinator.com/item?id=31903076)  
      DALL-E mini + other suggestions.  
      [craiyon.com](https://www.craiyon.com/) they created an online service.
- [ ] [ycombinator.com magamig.github.io](https://news.ycombinator.com/item?id=30613745)
      Image alignment.

## machina ex deo

- [x] [tailscale.com](https://tailscale.com/)  
      Access home from outside, works wonderfully!
- [ ] [free-for.dev](https://free-for.dev/)  
      Free stuff!
- [ ] [github.com/spantaleev/matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy)  
      Deploy a self-hosted matrix server.

## puzzles

- [x] [regexcrossword](https://regexcrossword.com/)  
      Crosswords with regexes! Using old gmail.

## games (one can dream)

- [ ] [gloomhaven](https://store.steampowered.com/app/780290/Gloomhaven/)
- [ ] [slipways](https://slipways.net/)
- [ ] [remnants of the precursors](https://rayfowler.itch.io/remnants-of-the-precursors)  
       masters of orion update
- [ ] [death trash](https://store.steampowered.com/app/941460/Death_Trash/)
- [x] [mindustry](https://mindustrygame.github.io/)  
      factorio tower defense fun
- [ ] [tactics ogre reborn](https://www.nintendo.com/store/products/tactics-ogre-reborn-switch/)
